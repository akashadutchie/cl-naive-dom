(in-package :common-lisp-user)

(defpackage :cl-naive-dom
  (:use :cl :cl-getx)
  (:export

   ;;tags
   :*tags*
   :default-transform

   ;;docs
   :attribute
   :set-attribute
   :key
   :value

   :tag
   :attributes
   :children
   :tag-convert
   :tag-equal-p

   :element
   :dynamic
   :keywordize

   :tag-p

   :*element*
   :*attributes*
   :*new-children*

   :att
   :atts
   :new-children

   :parse

   :emit-dom
   :emit-element

   :expand

   :query-selector

   :persist
   :load-dom
   
   
   ;;Macros
   :deftag
   :with-dom
   :with-sexp

   ;; chains of transform:
   :path
   :selector
   :find-transform-named
   :function-selector
   :function-selector-predicate
   :tag-selector
   :tag-selector-tags
   :select
   :transform
   :transform-name
   :transform-selector
   :transform-function
   :perform-transform
   :define-transform
   :apply-transform-chain
   ;; for debugging:
   :trace-transform
   :untrace-transform
   ;; conditions:
   :no-such-transform
   :no-such-transform-name
   :invalid-transform-lambda-list
   :invalid-transform-lambda-list-lambda-list
   :invalid-transform-lambda-list-transform-name

   ;; utility

   :extract
   :set-equal
   :attribute-equal-p
   :element-equal-p
   :copy-attribute
   :copy-element))
