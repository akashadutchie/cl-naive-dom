(in-package :cl-naive-dom)

(defclass attribute ()
  ((key :initarg :key
        :initform nil
        :accessor key
        :documentation "The key of the attribute. Must be a KEYWORD.")
   (value :initarg :value
          :initform nil
          :accessor value
          :documentation "The value of an attribute. When using the DSL with WITH-DOM the value can be a form."))
  (:documentation "Elements have attributes."))

(defgeneric tag (object)
  (:documentation "Returns a tag which itself or the car of what is passed.")
  (:method ((object t))
    object)
  (:method ((object cons))
    (car object)))

(defgeneric attributes (object)
  (:documentation "TODO:??? Not sure what the purpose of this...")
  (:method ((object t))
    '())
  (:method ((object cons))
    (loop :for (key value) :on (cdr object) :by (function cddr)
          :while (keywordp key)
          :collect key
          :collect value)))

(defgeneric children (object)
  (:documentation "TODO:??? Not sure what the purpose of this...")

  (:method ((object t)) '())
  (:method ((object cons))
    (loop :for current := (cdr object) :then (cddr current)
          :while (keywordp (car current))
          :finally (return current))))

(defclass element ()
  ((tag :initarg :tag
        :initform nil
        :accessor tag
        :documentation "The tag used for the element.")
   (attributes :initarg :attributes
               :initform nil
               :accessor attributes
               :documentation "The attributes of the element.")
   (children :initarg :children
             :initform nil
             :accessor children
             :documentation "The children of the element that are
             other elements."))
  (:documentation "A document is made up by a hierarchy of elements, ie
    the DOM. There is no document class per se, the root element of a
    hierarchy represents a document conceptually."))

(defun attribute (element key)
  "Used to get an attribute value from the attributes of an element."
  (let ((att (find key (attributes element) :key 'key :test 'equalp)))
    (when att
      (value att))))

(defun set-attribute (element key value)
  "Used to get an attribute value from the attributes of an element."
  (let ((att (find key (attributes element) :key 'key :test 'equalp)))
    (if att
        (setf (value att) value)
        (prog1
            (setf att (make-instance 'attribute :key key :value value))
          (push att (attributes element))))))

(defun dynamic (sexp)
  "Used in DEFTAG to build tags at run time using the DSL syntax and
when reading from a dom from file.

In DEFTAG the alternative is to use the dom to create custom tags
directly on the fly."
  (eval (apply 'cl-naive-dom::parse (list sexp))))

(defun keywordize (string)
  "Convenience function to create keywords, typically used when
creating dynamic tags."
  (intern (string-upcase string) :KEYWORD))

(defgeneric resolve-tag (tag attributes children)
  (:documentation "Resolves tag to an element instance."))

(defmethod resolve-tag (tag attributes children)
  (make-instance
   'element
   :tag (tag-convert tag)
   :attributes (loop
                 :with attrs := nil
                 :for att :on attributes :by #'cddr
                 :if (keywordp (first att))
                 :do (if (equal (first att) :atts%)
                         (if (and (consp (second att))
                                  (keywordp (first (second att))))
                             (loop
                               :for attx on (second att) by #'cddr
                               :if (keywordp (first attx))
                               :do (push (make-instance 'attribute
                                                        :key (first attx)
                                                        :value (second attx))
                                         attrs))
                             (setf attrs (append attrs (second att))))
                         (push (make-instance 'attribute
                                              :key (first att)
                                              :value (second att))
                               attrs))
                 :finally (return attrs))
   :children (if (listp children)
                 children
                 (list children))))

(defun process-tag (sexp)
  "Returns values: tag, attributes and body. It is used by the parser."
  (loop :with tag := (tag-convert (first sexp))
        :for rest :on (cdr sexp) :by #'cddr
        :while (keywordp (first rest))
        :collect (first  rest) :into attrs
        :collect (second rest) :into attrs
        :finally
        (return (values tag attrs rest))))

(defun enlist (forms)
  "
FORMS:  a list of lisp expressions, that may return atoms, or lists when evaluated.
RETURN: an expression that will append all the atoms, and the elements in the lists, into a list.
"
  (flet ((enlist-form (form)
           (cond
             ((symbolp form)
              `(uiop:ensure-list ,form))
             ((atom form)           ; other atoms are self-evaluating:
              `(list ,form))
             (t
              `(uiop:ensure-list ,form)))))
    (cond
      ((null forms)  'nil)
      ((null (cdr forms))
       (enlist-form (car forms)))
      (t
       `(concatenate 'list ,@(mapcar (function enlist-form) forms))))))

(defun declarationp (form)
  (and (consp form) (eq 'declare (first form))))

(define-condition simple-program-error (program-error simple-error)
  ())

(defun parse-body (where body)
  "
WHERE:          (member :lambda :locally :progn) specifies where the
                body is found, that is whether it may contains
                docstrings and declarations, or just declarations, or
                none.

BODY:           A list of forms.

RETURN:         Three values: a list containing one docstring or nil,
                a list of declarations, a list of forms.
"
  (flet ((progn-body (body)
           (if (some (lambda (form) (and (consp form) (eq 'declare (first form))))
                     body)
               (error 'simple-program-error
                      :format-control "Found a declaration in the a progn body: ~S"
                      :format-arguments (list body))
               body)))
    (ecase where
      ((:lambda)
       ;; {declaration} [docstring declaration {declaration}] {form}
       ;; {declaration} [docstring] form {form}
       (loop
         :with docstring    = nil
         :with declarations = '()
         :with actual-body  = '()
         :with state        = :opt-decl
         :for form :in body
         :do (ecase state
               (:opt-decl
                (cond
                  ((declarationp form) (push form declarations))
                  ((stringp form)      (setf docstring form
                                             state :seen-string))
                  (t                   (push form actual-body)
                                       (setf state :body))))
               ((:seen-string :after-decl)
                (if (declarationp form)
                    (progn (push form declarations)
                           (setf state :after-decl))
                    (progn (push form actual-body)
                           (setf state :body))))
               (:body
                (if (declarationp form)
                    (error 'simple-program-error
                           :format-control "Found a declaration ~S in the body: ~S"
                           :format-arguments (list form body))
                    (push form actual-body))))
         :finally (flet ((ensure-list (object)
                           (if (listp object)
                               object
                               (list object))))
                    (return (ecase state
                              (:opt-decl
                               (values (ensure-list docstring)
                                       declarations
                                       (nreverse actual-body)))
                              (:seen-string
                               (if actual-body
                                   (values (ensure-list docstring)
                                           declarations
                                           (nreverse actual-body))
                                   (values nil
                                           declarations
                                           (list docstring))))
                              ((:after-decl :body)
                               (values (ensure-list docstring)
                                       declarations
                                       (nreverse actual-body))))))))
      ((:locally)
       ;; {declaration} {form}
       (loop
         :for current :on body
         :for form = (car current)
         :while (declarationp form)
         :collect form :into declarations
         :finally (return  (values nil
                                   declarations
                                   (progn-body current)))))
      ((:progn)
       ;; {form}
       (values nil
               nil
               (progn-body body))))))

(defun parse (sexp &optional environment)
  "Parses a sexp and resolves the tags read to element instances."
  ;; TODO: parse: deal with the environment!!! MACROLET and SYMBOL-MACROLET extend the environment, and that must be recorded and passed for inner PARSE calls.
  (if (atom sexp)
      sexp
      (let ((op (car sexp)))
        (case op
          ((quote)
           sexp)
          ((function)
           (cond
             ((symbolp (second sexp))
              sexp)
             ((and (listp (second sexp))
                   (eq 'lambda (first (second sexp))))
              (destructuring-bind (lambda (&rest lambda-list) &body body) (second sexp)
                (multiple-value-bind (docstrings declarations body) (parse-body :lambda body)
                  `(function (,lambda ,lambda-list
                     ,@docstrings
                     ,@declarations
                     ,@(mapcar (function parse) body))))))
             (t (error "Unsupported FUNCTION extension: ~S" sexp))))
          ((progn)
           `(,op ,@(mapcar (function parse) (cdr sexp))))
          ((block)
           `(,op ,(second sexp)
                 ,@(mapcar (function parse) (cddr sexp))))
          ((catch)
           `(,op ,(second sexp) ; note we could also parse the catch tag, but elements as catch tag?
                 ,@(mapcar (function parse) (cddr sexp))))
          ((flet)
           `(,op ,(mapcar (lambda (fun)
                            (destructuring-bind (fname lambda-list &body body) fun
                              (multiple-value-bind (docstrings declarations body) (parse-body :lambda body)
                                `(,fname ,lambda-list
                                         ,@docstrings
                                         ,@declarations
                                         ,@(mapcar (function parse) body)))))
                          (second sexp))
                 ,@(with-disabled-tags (mapcar (function first) (second sexp))
                     (mapcar (function parse) (cddr sexp)))))
          ((labels)
           `(,op ,(loop
                    :for fun :in (second sexp)
                    :collect (first fun) :into disabled
                    :collect (destructuring-bind (fname lambda-list &body body) fun
                               (multiple-value-bind (docstrings declarations body) (parse-body :lambda body)
                                 `(,fname ,lambda-list
                                          ,@docstrings
                                          ,@declarations
                                          ,@(with-disabled-tags disabled
                                              (mapcar (function parse) body))))))
                 ,@(with-disabled-tags (mapcar (function first) (second sexp))
                     (mapcar (function parse) (cddr sexp)))))
          ((go) sexp)
          ((if)
           `(,op ,(parse (second sexp))
                 ,(parse (third sexp))
                 ,(parse (fourth sexp))))
          ((let let*)
           `(,op ,(mapcar (lambda (binding)
                            (if (atom binding)
                                binding
                                (list (first binding) (parse (second binding)))))
                          (second sexp))
                 ,@(multiple-value-bind (docstrings declarations body) (parse-body :locally (cddr sexp))
                     (declare (ignore docstrings))
                     `(,@declarations
                       ,@(mapcar (function parse) body)))))
          ((load-time-value)
           `(,op ,(parse (second sexp))))
          ((macrolet)
           `(,op ,(mapcar (lambda (fun)
                            (destructuring-bind (mname lambda-list &body body) fun
                              (multiple-value-bind (docstrings declarations body) (parse-body :lambda body)
                                `(,mname ,lambda-list
                                         ,@docstrings
                                         ,@declarations
                                         ,@(mapcar (function parse) body)))))
                          (second sexp))
                 ,@(with-disabled-tags (mapcar (function first) (second sexp))
                     (mapcar (function parse) (cddr sexp)))))
          ((multiple-value-call)
           ;; Note: we assume the function is not a tag.
           `(,op ,(second sexp)
                 ,@(mapcar (function parse) (cddr sexp))))
          ((multiple-value-prog1)
           `(,op ,(parse (second sexp))
                 ,@(mapcar (function parse) (cddr sexp))))
          ((progv)
           `(,op ,(second sexp)
                 ,(mapcar (function parse) (third sexp))
                 ,@(mapcar (function parse) (cdddr sexp))))
          ((return-from)
           `(,op ,(second sexp)
                 ,(parse (third sexp))))
          ((setq)
           `(,op ,@(loop :for (var val) :on (cdr sexp) :by (function cddr)
                         :collect var
                         :collect (parse val))))
          ((symbol-macrolet)
           `(,op ,(mapcar (lambda (binding)
                            (list (first binding)
                                  (parse (second binding))))
                          (second sexp))
                 ,@(mapcar (function parse) (cddr sexp))))
          ((tagbody)
           `(,op
             ,@(loop :for item :in (cdr sexp)
                     :if (atom item)
                     :collect item
                     :else
                     :collect (parse item))))
          ((the)
           `(,op ,(second sexp)
                 ,(parse (third sexp))))
          ((throw)
           `(,op ,(second sexp)
                 ,(parse (third sexp))))
          ((unwind-protect)
           `(,op ,@(mapcar (function parse) (cdr sexp))))
          (otherwise
           ;; tag, macro or function:
           (cond
             ((and (symbolp op) (fboundp op) (macro-function op))
              ;; macro
              (parse (macroexpand sexp environment)))
             ((or (keywordp (car sexp))
                  (and (symbolp (car sexp)) (tag-p (car sexp))))
              ;; tag
              (multiple-value-bind (tag attrs body) (process-tag sexp)
                (let ((attrs% (loop :for (key value) :on attrs :by (function cddr)
                                    :if (eql key :atts%)
                                    ;; will be evaluated in resolve-tag
                                    :collect key :into attribs :and :collect value
                                    :into attribs
                                    :else
                                    :collect key :into attribs :and :collect (parse value)
                                    :into attribs
                                    :finally (return (if (null attribs)
                                                         'nil
                                                         `(list ,@attribs)))))
                      (body%  (mapcar #'parse body)))
                  `(resolve-tag ',tag ,attrs% ,(enlist body%)))))
             (t
              ;; assume function
              `(,op ,@(mapcar (function parse) (cdr sexp))))))))))

(defgeneric emit-element (type tag element path &key &allow-other-keys)
  (:documentation "This method should be implemented to emit different
  document formats. An emit should produce what ever needs to be
  written to file to represent the document format, like html for
  instance."))

(defmethod emit-element ((type (eql :sexp)) tag element path &key (keywords-p t) &allow-other-keys)
  (declare (ignore tag))
  (let ((sexp))
    (push (if keywords-p
              (keywordize (tag element))
              (tag element))
          sexp)
    (dolist (att (reverse (attributes element)))
      (when (value att)
        (push (key att)
              sexp)
        (push (value att)
              sexp)))
    (dolist (child (children element))
      (push (emit-dom type child path :keywords-p keywords-p)
            sexp))
    (reverse sexp)))

(defgeneric emit-dom (document-format element path &key &allow-other-keys)
  (:documentation "Method that needs to be implimented to a specific
  document format like html or pdf."))

(defmethod emit-dom (document-format element path &rest rest &key &allow-other-keys)
  (etypecase element
    (string
     element)
    (number
     element)
    (keyword
     element)
    (element
     (push element path)
     (apply #'emit-element document-format (tag element) element path rest))
    (list
     (mapcar (lambda (elm)
               (apply #'emit-dom document-format elm path rest))
             element))))

(defgeneric expand (tree)
  (:documentation "Expand will collect all the tags in the DOM tree, sort topologically
by deepest depth, and build a chain of tag transforms, that is then
applied to the DOM.  The operation is repeated until no further
transformation is possible.

Expand must check whether there are cycles in the results of the tag
transforms, and signal an error in that case. ie. if :foo expands to
:bar, and :bar expands to :foo, the last :foo we'd have an infinite
loop, so we signal an error."))

(defmethod collect-tags ((element element))
  (let ((tags '()))
    (labels ((collect (element)
               (when (typep element 'element)
                 (pushnew (tag element) tags)
                 (mapcar (function collect) (children element)))))
      (collect element)
      tags)))

;; TODO: expand: cycle detection is missing.
(defmethod expand ((element element))
  "Expands an Element/DOM"
  (loop
    :for tags := (collect-tags element)
    :for transforms := (remove nil (mapcar (function default-transform) tags))
    :while transforms
    :do (setf element (apply-transform-chain transforms element)))
  element)

(defmethod expand ((elements list))
  "Expands a list of elements."
  (mapcar (function expand) elements))

(defmethod print-object ((object attribute) stream)
  (print-unreadable-object (object stream :type t :identity t)
    (prin1 (key object) stream)
    (princ " " stream)
    (prin1 (value object) stream))
  object)

(defmethod print-object ((object element) stream)
  (print-unreadable-object (object stream :type t :identity t)
    (format stream "~S ~S ~{~S~^ ~}" (tag object) (attributes object) (children object)))
  object)

(defgeneric query-select-element (element path func &key &allow-other-keys)
  (:documentation "Applies selector func to check if element qualifies. "))

(defparameter *query-elements* nil)

(defun query-selector% (element path func &key &allow-other-keys)
  (etypecase element
    (string)
    (number)
    (keyword)
    (element
     (query-select-element element (cons element path) func))
    (list
     (dolist (elm element)
       (query-selector% elm path func)))))

(defmethod query-select-element (element path func &key &allow-other-keys)
  "Returns an element from the dom (ELEMENT) using the FUNC function.
FUNC should a take two parameters ELEMENT and PATH."
  (let ((elm (funcall func element path)))
    (when elm
      (push elm *query-elements*))

    ;;The value of an attribute could also be an element.
    (mapc (lambda (att)
            (query-selector% (value att) path func))
          (attributes element))
    (mapc (lambda (element)
            (query-selector% element path func))
          (children element))))

(defgeneric query-selector (element func &key &allow-other-keys)
  (:documentation "Used to find elements in a dom."))

(defmethod query-selector (element func &key &allow-other-keys)
  "Returns a list of elements from the dom (ELEMENT) using the FUNC function.
FUNC should a take two parameters ELEMENT and PATH."
  (let ((*query-elements* nil))
    (query-selector% element nil func)
    (nreverse *query-elements*)))

(defgeneric persist (element file &key)
  (:documentation "Persists an element/DOM to file."))

(defmethod persist (element file &key keywords-p)
  "Writes the DOM out as a sexp, note that by default converted tag names are used to make sure any package meta data is also written out."
  (with-open-file (stream file :direction :output
                               :if-exists :supersede
                               :if-does-not-exist :create)
    ;;Level the playing field.
    (with-standard-io-syntax
      (write (emit-dom :sexp element nil :keywords-p keywords-p)
             :stream stream))))

(defgeneric load-dom (file)
  (:documentation "Reads and parses the file to produce a loaded DOM.

Expansions are not done at this time, you need to expand after the load-file.

If the DOM was written with keywords-p set to nil then you need to
define the tags that are used before trying to load the DOM, else the
parser wont successfully parse the file contents."))

;;TODO: Will have issues if load-dom is called at compile time?
(defmethod load-dom (file)
  (with-open-file (stream file :direction :input)
    (dynamic (with-standard-io-syntax
               (let ((*read-eval* nil))
                 (read stream))))))
