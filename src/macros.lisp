(in-package :cl-naive-dom)

(defmacro with-dom (&body body)
  "Macro used to wrap the custom DSL code with, it parses the DSL to a
DOM."
  (parse `(progn ,@body)))

(defmacro deftag (name-or-name-and-options &body dom-body)
  "Creates a TAG.

NAME-OR-NAME-AND-OPTIONS:
             Either a symbol naming the tag, or a list destructurable to either:
                 (tag) ; or
                 (tag (&rest transform-lambda-list) &optional selector)
             in which case TAG is the name of the tag, and:

TRANSFORM-LAMBDA-LIST:
             Introduces a lambda-list for dom-thunk.
             This can be either a list of 1 or 2 mandatory parameters:
             (<element>) or (<element> <new-children>)
             or 3 or 4 mandatory parameters: (<tag> <attributes> <children>)
             or (<tag> <attributes> <children> <new-chilren>), in which
             case the element is destructured into tag, attributes and children.
             Optionally, a &key path parameter may be added.
             If a name for the <new-children> parameter is not given, then the
             symbol CL-NAIVE-DOM::NEW-CHILDREN is used.
             If no transform-lambda-list is provided, then
             (CL-NAIVE-DOM::TAG CL-NAIVE-DOM::ATTRIBUTES CL-NAIVE-DOM::CHILDREN
              CL-NAIVE-DOM::NEW-CHILDREN) is used.


SELECTOR:    A function used to select the tag from the dom when doing
             expansion.

DOM-BODY:    A body parsed WITH-DOM, to transform the TAG elements.
"
  (flet ((generate-transform (tag transform-lambda-list selector dom-body)
           (let ((final-lambda-list (or
                                     (if (= (length transform-lambda-list) 1)
                                         (list (car transform-lambda-list) 'new-children)
                                         transform-lambda-list)
                                     '(tag attributes children new-children &key path)))
                 (tag (tag-convert tag)))
             (when (and dom-body (not selector))
               (setf selector (list tag)))
             `(progn
                (eval-when (:compile-toplevel)
                  (ensure-tag ',tag)
                  (declaim (ftype function ,tag)))
                (eval-when (:load-toplevel :execute)
                  (ensure-tag ',tag)
                  ,@(when dom-body
                      `((define-transform (,tag ,selector
                                           ,final-lambda-list)
                          (declare (ignorable ,@(remove '&key final-lambda-list)))
                          ,@dom-body)))
                  ',tag)))))
    (cond
      ((symbolp name-or-name-and-options)
       (generate-transform name-or-name-and-options nil nil dom-body))
      ((and (listp name-or-name-and-options) (= 1 (length name-or-name-and-options)))
       (generate-transform (first name-or-name-and-options) nil nil dom-body))
      (t
       (destructuring-bind (tag (&rest transform-lambda-list) &optional selector) name-or-name-and-options
         (generate-transform tag transform-lambda-list selector dom-body))))))

;; Examples of macro-expansion:

;; (pprint (macroexpand-1 '(deftag :custom (:span "Custom Tag"))))
;;
;; (progn (eval-when (:compile-toplevel) (ensure-tag 'custom) (declaim (ftype function custom)))
;;        (eval-when (:load-toplevel :execute)
;;          (ensure-tag 'custom)
;;          (define-transform
;;            (custom (custom) (tag attributes children new-children &key path))
;;            (declare (ignorable tag attributes children new-children path))
;;            (:span "Custom Tag"))
;;          'custom))
;;
;; (pprint (macroexpand-1 '(deftag (:custom) (:span "Custom Tag"))))
;;
;; (progn (eval-when (:compile-toplevel) (ensure-tag 'custom) (declaim (ftype function custom)))
;;        (eval-when (:load-toplevel :execute)
;;          (ensure-tag 'custom)
;;          (define-transform
;;            (custom (custom) (tag attributes children new-children &key path))
;;            (declare (ignorable tag attributes children new-children path))
;;            (:span "Custom Tag"))
;;          'custom))
;;
;; (pprint (macroexpand-1 '(deftag (:custom ()) (:span "Custom Tag"))))
;;
;; (progn (eval-when (:compile-toplevel) (ensure-tag 'custom) (declaim (ftype function custom)))
;;        (eval-when (:load-toplevel :execute)
;;          (ensure-tag 'custom)
;;          (define-transform
;;            (custom (custom) (tag attributes children new-children &key path))
;;            (declare (ignorable tag attributes children new-children path))
;;            (:span "Custom Tag"))
;;          'custom))

(defmacro with-sexp (&body body)
  "Emits sexp."
  `(emit-dom :sexp (with-dom ,@body) nil))
