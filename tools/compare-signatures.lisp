(cl:in-package "CL-USER")

(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload :swank :verbose nil :silent t)
  (ql:quickload :com.informatimago.lispdoc :verbose nil :silent t)
  (ql:quickload :cl-ppcre :verbose nil :silent t))

(defpackage "COMPARE-SIGNATURES"
  (:use "COMMON-LISP"
        "COM.INFORMATIMAGO.LISPDOC.DOC"
        "COM.INFORMATIMAGO.COMMON-LISP.CESARUM.PACKAGE")
  (:export "EXTRACT-DOCUMENTATION-SIGNATURES"
           "REPORT-DOCUMENTATION-PROBLEMS"))
(in-package "COMPARE-SIGNATURES")

(defgeneric extract-documentation-signatures (what-from))

(defmacro collecting (&body body)
  (let ((vresult (gensym "result")))
    `(let ((,vresult '()))
       (flet ((collect (item) (push item ,vresult)))
         ,@body)
       ,vresult)))

(defun intern-in-keyword (text)
  (intern (substitute #\- #\space (string-upcase text)) (load-time-value (find-package "KEYWORD"))))

(defun remove-org-markup&read-from-string (text)
  (read-from-string
   (if (and (char= #\= (aref text 0))
            (char= #\= (aref text (1- (length text)))))
       (subseq text 1 (- (length text) 1))
       text)))

(defmethod extract-documentation-signatures ((documentation-pathname pathname))
  "Read the org-file at DOCUMENTATION-PATHNAME line by line,
and return a list of the kind, name and optional signatures
of each documented item following the usual format, such as:

*** [macro] deftag ((tag &key environment selector with) &body dom-body)

RETURN: a list of documented item kind-name-signatures lists.
"
  (collecting
   (with-open-file (docs documentation-pathname)
     (loop
       :for line := (read-line docs nil docs)
       :until (eql line docs)
       :do (multiple-value-bind (start end starts ends)
               (cl-ppcre:scan
                #.(concatenate 'string
                               "^\\** *"
                               "\\[([-a-z]+)\\]"
                               " *"
                               "([^ ]+)"
                               "("
                               " *"
                               "(\\((.*)\\))"
                               ")?"
                               " *"
                               "$"
                               )
                line)
             (declare (ignore end))
             (when start
               (collect (loop
                          :for i :in '(0 1 3)
                          :for cleanup :in '(intern-in-keyword remove-org-markup&read-from-string read-from-string)
                          :for start := (aref starts i)
                          :for end := (aref ends i)
                          :when start
                          :collect (let ((text (subseq line start end)))
                                     (handler-case (funcall cleanup text)
                                       (:no-error (object &rest ignored) (declare (ignore ignored)) object)
                                       (error () text)))))))))))

(defmethod extract-documentation-signatures ((package package))
  (collecting
   (dolist (packdoc (COM.INFORMATIMAGO.LISPDOC:lispdoc (list package)))
     (collect (list :package (package-name (doc-symbol packdoc))))
     (dolist (symdoc (packdoc-external-symbol-docs packdoc))
       (collect (typecase symdoc
                  (fundoc   (list (if (eq :undocumented (doc-kind symdoc))
                                      :function
                                      (doc-kind symdoc))
                                  (doc-symbol symdoc)
                                  (fundoc-lambda-list symdoc)))
                  (classdoc (list (if (eq :undocumented (doc-kind symdoc))
                                      :class
                                      (doc-kind symdoc))
                                  (doc-symbol symdoc)
                                  (classdoc-precedence-list symdoc)))
                  (t        (list (doc-kind symdoc)
                                  (doc-symbol symdoc)))))))))


(defun print-items (items &optional (print-kind-p t))
  (dolist (item items)
    (destructuring-bind (doc-kind doc-symbol &optional (doc-list nil doc-list-p)) item
      (format t "    ~:[~*~;~A ~]~S~:[~*~; ~S~]~%" print-kind-p doc-kind doc-symbol doc-list-p doc-list))))

(defun equivalent-item-kind (a b)
  (let ((equivalences '((:reader :generic-function :function)
                        (:writer :generic-function :function)
                        (:accessor :generic-function :function)
                        (:generic :generic-function)
                        (:special :variable))))
    (or (eql a b)
        (loop :for class :in equivalences
              :thereis (and (member a class)
                            (member b class))))))

(defun equal-item (a b)
  (and (eql (second a) (second b))
       (equivalent-item-kind (first a) (first b))))


;; Note: for now we use equalp, but we could be more laxist.
;; 1- ignore the name of the parameters (but warn if mismatch)
;; 2- accept innocuous lambda-list variations.
;; 3- accept innocuous precedence-list variations.

(defun matching-function-signature-p (lld lls)
  (equalp lld lls))

(defun matching-macro-signature-p (lld lls)
  (equalp lld lls))

(defun matching-precedence-list-p (pld pls)
  ;; ignore a final T
  ;; ignore missing intermediate classes in pld
  (cond
    ((equalp '(t) (last pld))
     (matching-precedence-list-p (butlast pld) pls))
    ((equalp '(t) (last pls))
     (matching-precedence-list-p pld (butlast pls)))
    ((equalp pld pls))
    (t
     (loop
       :for current :in pld
       :always (let ((match (member current pls)))
                 (and match
                      (setf pls match)
                      t))))))

(defun test/matching-precedence-list-p ()
  (assert (and (matching-precedence-list-p '(a b c) '(a b c))
               (matching-precedence-list-p '(a b c t) '(a b c))
               (matching-precedence-list-p '(a b c) '(a b c t))
               (matching-precedence-list-p '(a b c t) '(a b c t))
               (matching-precedence-list-p '(b c) '(a b c))
               (matching-precedence-list-p '(a c) '(a b c))
               (matching-precedence-list-p '(a b) '(a b c))
               (matching-precedence-list-p '(a c e) '(a b c d e))
               (matching-precedence-list-p '(b d) '(a b c d e))))
  :success)

(defvar *docs* '() "Documented symbols.")
(defvar *srcs* '() "Symbols with in documented package.")
(defvar *exports* '()  "Exported symbols from the documented package.")
(defvar *unexported* '()
  "List of documented symbols that are not exported from the current package.")
(defvar *undocumented* '()
  "List of symbols exported from the current package that are not documented.")

(defun report-documentation-problems (documentation-pathname documented-package
                                      &key explore)
  (let ((*package* documented-package))
    (let ((docs (extract-documentation-signatures documentation-pathname))
          (srcs (extract-documentation-signatures documented-package))
          (exports (package-exports documented-package)))
      (let ((unexported (collecting
                         (dolist (item docs)
                           (destructuring-bind (doc-kind doc-symbol &optional (doc-list nil doc-list-p)) item
                             (declare (ignore doc-kind doc-list doc-list-p))
                             (unless (member doc-symbol exports)
                               (collect item)))))))
        (when unexported
          (format t "~%Documented symbols that are not exported from ~S:~2%"
                  (package-name documented-package))
          (print-items unexported)
          (when explore
            (let ((*package* *package*)
                  (*docs* docs)
                  (*srcs* srcs)
                  (*exports* exports)
                  (*unexported* unexported))
              (com.informatimago.common-lisp.interactive.interactive:repl)))))

      (let ((undocumented (remove :undocumented srcs :key (function first)
                                  :test-not (function eql))))
        (when undocumented
          (format t "~%Symbols exported from ~S that are not documented:~2%"
                  (package-name documented-package))
          (print-items (sort undocumented (function string<) :key (function second)) nil)
          (when explore
            (let ((*package* *package*)
                  (*docs* docs)
                  (*srcs* srcs)
                  (*exports* exports)
                  (*undocumented* undocumented))
              (com.informatimago.common-lisp.interactive.interactive:repl)))))
      
      (let ((documented-srcs (remove :undocumented srcs :key (function first))))
        (let ((nods (set-difference docs documented-srcs :test (function equal-item))))
          (when nods
            (format t "~%Symbols exported from ~S that are documented but have no docstring (ignore them):~2%"
                    (package-name documented-package))
            (print-items (sort nods (function string<) :key (function second)))))
        (let ((unds (set-difference documented-srcs docs :test (function equal-item))))
          (when unds
            (format t "~%Symbols exported from ~S that have a docstring but are not documented:~2%"
                    (package-name documented-package))
            (print-items (sort unds (function string<) :key (function second)))))
        (let ((both (intersection docs documented-srcs :test (function equal-item))))
          (when both
            (collecting
             (dolist (item both)
               (let ((doc (find item docs            :test (function equal-item)))
                     (src (find item documented-srcs :test (function equal-item))))
                 (if (equal-item doc src)
                     ;; find both signatures and compare them:
                     (case (first src)
                       ((:reader :writer :accessor :generic-function :function)
                        (unless (matching-function-signature-p (third doc) (third src))
                          (format t "~%The signature of ~A ~S doesn't match in documentation and sources:~%  ~S~%  ~S~%"
                                  (first doc) (second doc) (third doc) (third src))))
                       ((:macro)
                        (unless (matching-macro-signature-p (third doc) (third src))
                          (format t "~%The signature of ~A ~S doesn't match in documentation and sources:~%  ~S~%  ~S~%"
                                  (first doc) (second doc) (third doc) (third src))))
                       ((:class)
                        (unless (matching-precedence-list-p (third doc) (third src))
                          (format t "~%The precedence list of ~A ~S doesn't match in documentation and sources:~%  ~S~%  ~S~%"
                                  (first doc) (second doc) (third doc) (third src))))
                       (otherwise #| all is good |#))
                     ;; else
                     (format t "Matching problem with ~S~%" item))))))))))
  (terpri)
  (values))




