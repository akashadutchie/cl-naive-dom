(defsystem "cl-naive-dom.abt"
  :description "Simple tool to write angle bracket tags."
  :version "2022.3.17"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:cl-naive-dom)
  :components (
               (:file "src/abt/package")
               (:file "src/abt/abt" :depends-on ("src/abt/package"))))
