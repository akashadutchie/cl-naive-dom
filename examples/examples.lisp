(require :cl-naive-dom)
(require :cl-naive-dom.abt)
(require :cl-naive-dom.js)
(defpackage :naive-examples (:use :cl :cl-naive-dom :cl-naive-dom.abt :cl-naive-dom.js))
(in-package :naive-examples)

;; Define DOM

(with-dom
  (:cv
   (:name "James Tiberius Kirk")
   (:birthdate "2233/03/22")))

;; Define Tag

(deftag :box)

;; Define Tag with Expand

(deftag :box
  (:div
   (:span (att :title))
   (:div (new-children))))

(default-transform :box)

;;Define a Dynamic Tag
(deftag (:h ())
  (cl-naive-dom:dynamic `(,(keywordize (format nil "h~A" (att :size)))
                          ,@(new-children))))

(with-sexp
  (with-dom
    (:div
     (:h :size 1 "aaa"))))

;;(:DIV (:H :SIZE 1 "aaa"))

(with-sexp
  (expand
   (with-dom
     (:div
      (:h :size 1 "aaa")))))

;;(:DIV (:H1 "aaa"))

;;Defines Tag with ELEMENTS
(deftag (:dynamic-expand-tag ())
  (make-instance
   'element
   :tag :div
   :attributes (list
                (make-instance 'attribute
                               :key :width
                               :value (att :width)))
   :children (list
              (make-instance 'element
                             :tag :span
                             :attributes (list
                                          (make-instance 'attribute
                                                         :key :class
                                                         :value (att :class)))
                             :children (new-children)))))
(let ((dom
        (expand
         (let ((x 1))
           (with-dom
             (:div
              (::dynamic-expand-tag :width x
                                    "Hello World")))))))
  (with-sexp
    dom))

;;(:DIV (:DIV :WIDTH 1 (:SPAN "Hello World")))

;;Expand a DOM

(with-sexp
  (expand
   (with-dom
     (:box :title "The Box"
           (:span "The content of the box.")))))

;;Emit a DOM

(emit-dom :sexp
          (with-dom
            (:cv
             (:name "James Tiberius Kirk")
             (:birthdate "2233/03/22")))
          nil)

;;Emit Angle Brackets
(with-abt
  (with-dom
    (:cv
     (:name "James Tiberius Kirk")
     (:birthdate "2233/03/22"))))

;;Query DOM

(defparameter *cv*
  (with-dom
    (:document
     :title "My CV"
     :last-update "2000"
     :name "Piet"
     :surname "Gieter"
     :jobs
     (:job
      :job-title "Bottle Washer"
      :when "1994"
      :duties (list "Washing bottles."
                    "Washing more bottles."))
     (:job
      :job-title "Chief Cook"
      :when "1994"
      :duties (list "Cook food."
                    "Cook more food."))
     (:summary "Its been a colourful career of menial jobs"))))

(query-selector
 *cv*
 (lambda (element path)
   (declare (ignore path))
   (when (and (tag-equal-p element :job)
              (equal (attribute element :when) "1994"))
     element)))

;;Query DOM to return non elements

(defparameter *cv*
  (with-dom
    (:document
     :title "My CV"
     :last-update "2000"
     :name "Piet"
     :surname "Gieter"
     :jobs
     (:job
      :job-title "Bottle Washer"
      :when "1994"
      :duties (list "Washing bottles."
                    "Washing more bottles."))
     (:job
      :job-title "Chief Cook"
      :when "1994"
      :duties (list "Cook food."
                    "Cook more food."))
     (:summary "Its been a colourful career of menial jobs"))))

(query-selector
 *cv*
 (lambda (element path)
   (declare (ignore path))
   (when (and (tag-equal-p element :job)
              (equal (attribute element :when) "1994"))
     (list :job-tile (attribute element :job-title)))))

;;((:JOB-TILE "Bottle Washer") (:JOB-TILE "Chief Cook"))

;;Trasform a DOM

(deftag :cv)

(define-transform (cv-to-html (:cv) (element new-children))
  (declare (ignore new-children))
  (:div
   (:h2 "CV")
   (:div
    (:div "NAME")
    (:div (extract :name element)))))

(transform cv-to-html)

(let ((dom (with-dom
             (:cv
              (:name "James Tiberius Kirk")
              (:birthdate "2233/03/22")))))
  (with-sexp (perform-transform 'cv-to-html dom)))

;;(:DIV (:H2 "CV") (:DIV (:DIV "NAME") (:DIV (:NAME "James Tiberius Kirk"))))

;;Apply Transform Chain

(deftag :cv)

(define-transform (cv-to-html-step-1 (:cv) (element new-children))
  (declare (ignore new-children))
  (car (extract :name element)))

(define-transform (cv-to-html-step-2 (:name) (element new-children))
  (declare (ignore new-children))
  (:div
   (:h2 "CV")
   element))

(let ((dom (with-dom
             (:cv
              (:name "James Tiberius Kirk")
              (:birthdate "2233/03/22")))))

  (with-sexp
    (apply-transform-chain '(cv-to-html-step-1
                             cv-to-html-step-2)
                           dom)))
#|
(:DIV
(:H2 "CV")
(:NAME "James Tiberius Kirk"))
|#

;; Emit JS

(let ((dom (with-dom
             (:div :class "some-class"
                   (:h2 "The titles")
                   (:div "The Body")))))
  (with-js dom))

#|

"var EL776 = document.createElement(div);
EL776.setAttribute('class', 'some-class');
var EL815 = document.createElement(h2);

EL815.innerHTML += 'The titles ';
EL776.appendChild(EL815);
var EL816 = document.createElement(div);

EL816.innerHTML += 'The Body ';
EL776.appendChild(EL816);
document.body.appendChild(EL776);"
|#

;; Use code in the DSL

(deftag (:big)
  (let ((text "Hello")
        (width 10)
        (class "b")
        (elements))

    (dotimes (i (att :size))
      (push
       (:div
        (:span "a")
        (:span "b")
        (:custom-c :width width :class class text "there!")
        (:custom-c :width width :class class text "there!"))
       elements))
    elements))

;;10000

;;** I have the NEED for SPEED!

(deftag (:big)
  (let ((text "Hello")
        (width 10)
        (class "b")
        (elements))

    (dotimes (i (att :size))
      (push
       (:div
        (:span "a")
        (:span "b")
        (:custom-c :width width :class class text "there!")
        (:custom-c :width width :class class text "there!"))
       elements))
    elements))

(time
 (length
  (children
   (expand
    (with-dom
      (:div
       (:big :size 10000)))))))

(time
 (length
  (with-abt
    (expand
     (with-dom
       (:div
        (:big :size 10000)))))))

(time
 (length
  (with-js
    (expand
     (with-dom
       (:div
        (:big :size 10000)))))))

