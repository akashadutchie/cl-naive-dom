(defsystem "cl-naive-dom"
  :description "Document Object Model (DOM) with a simple tag-based extendable Domain
Specific Language (DSL). Usable in itself and to emit different document formats like HTML etc."
  :version "2022.8.25"
  :author "Phil Marneweck, Pascal J. Bourguignon"
  :licence "MIT"
  :depends-on (:cl-getx :alexandria :split-sequence)
  :components ((:file "src/package")
               (:file "src/tags" :depends-on ("src/package"))
               (:file "src/docs"        :depends-on ("src/tags"))
               (:file "src/macros"      :depends-on ("src/package" "src/docs"))
               (:file "src/transforms"  :depends-on ("src/package" "src/docs"))
               (:file "src/utility"     :depends-on ("src/package" "src/docs"))))

