(in-package :cl-naive-dom.tests)

(defun get-temp ()
  (handler-case
      (cl-fad::get-default-temporary-directory)
    (error (c)
      (declare (ignore c))
      (make-pathname :directory '(:absolute "tmp")))))

(defun temp-file (file-name)
  (cl-fad:merge-pathnames-as-file
   (get-temp)
   file-name))

(defmacro with-current-package (&body body)
  `(let ((*package* (or (find-package #.(package-name *package*))
                        (error "Cannot find the package named ~S"
                               #.(package-name *package*)))))
     ,@body))

(deftag (symbol-tag ())
  (:div (new-children)))

(with-dom
  (let ((symbol-tag))
    (declare (ignore symbol-tag))
    (symbol-tag)))

(deftag (:custom ())
  (:span "Custom Tag"))

(emit-dom :sexp
          (expand
           (with-dom
             (:div (:custom))))
          nil)

(deftag (:custom-alt ())
  (let ((body "Custom Tag - Alt"))
    (:div ;; transforms must return an ELEMENT.
     (:div "Multiple tag return.")
     (:span
      :atts% (atts)
      body))))

(emit-dom :sexp
          (expand
           (with-dom
             (:div (:custom-alt))))
          nil)

(deftag (:vid ())
  (:div (new-children)))

;;To build a tag at run time use dynamic.
(deftag (:h ())
  (cl-naive-dom::dynamic `(,(keywordize (format nil "h~A" (att :size)))
                           ,@(new-children))))

(deftag (:custom-a ())
  (make-instance
   'element
   :tag :div
   :attributes (list (make-instance 'attribute
                                    :key :width
                                    :value (att :width)))
   :children (list (make-instance 'element
                                  :tag :span
                                  :attributes (list (make-instance 'attribute
                                                                   :key :class
                                                                   :value (att :class)))
                                  :children (new-children)))))
;;defevtag
(deftag (:custom-b ())
  (:div :width (att :width)
        (:span :class (att :class)
               (new-children))))
;;defevtag
(deftag (:custom-c ())
  (make-instance
   'element
   :tag :div
   :attributes (list (make-instance 'attribute
                                    :key :width
                                    :value (att :width)))
   :children (list (make-instance 'element
                                  :tag :span
                                  :attributes (list (make-instance 'attribute
                                                                   :key :class
                                                                   :value (att :class)))
                                  :children (new-children)))))

(deftag (:curer ())
  (:p "doctor"))

(deftag (:curer ())
  (:p "veterinarian"))

(deftag (:custom ())
  (:span "Custom PJB Tag"))

(deftag (:custom ())
  (:span "Custom World Tag"))

;;TODO: sort out lambdalist
(deftag (:custom-1 (element new-children))
  (:ul
   (:li (:span
         :atts% (attributes element)
         new-children))
   (:li (:span
         :atts% (attributes element)
         new-children))))

;;TODO: sort out lambdalist
(deftag (:custom-3 (element children))
  (:ul
   (:li (:span
         :atts% (attributes element)
         children))
   (:li (:span
         :tag (string (tag element))
         :atts% (attributes element)
         children))))

(deftag (div ()))
(deftag (h1 ()))

(testsuite  :docs

  (testcase :deftag-with-lambda-list
            :expected '((:UL (:LI (:SPAN :CLASS "dwll" "In custom 1." "Done."))
                         (:LI (:SPAN :CLASS "dwll" "In custom 1." "Done.")))
                        (:UL (:LI (:SPAN :CLASS "dwll" "In custom 3." "Done."))
                         (:LI (:SPAN :CLASS "dwll" :TAG "CUSTOM-3" "In custom 3." "Done."))))
            :actual (with-current-package
                      (unwind-protect
                           (progn

                             (list (let ((dom (expand (with-dom (:custom-1 :class "dwll"
                                                                           "In custom 1."
                                                                           "Done.")))))
                                     (emit-element :sexp :custom-1 dom nil))
                                   (let ((dom (expand (with-dom (:custom-3 :class "dwll"
                                                                           "In custom 3."
                                                                           "Done.")))))
                                     (emit-element :sexp :custom-3 dom nil))))
                        #|;; Clean up:
                        ;; Nothing|#)))

  (testcase :parse
            :expected '(cl-naive-dom::resolve-tag
                        'cl-naive-dom.tests::div
                        (list :class "section" :test "YES")
                        (concatenate 'list
                         (uiop/utility:ensure-list
                          (cl-naive-dom::resolve-tag
                           'cl-naive-dom.tests::h1 nil
                           (list "Testing Docs")))
                         (uiop/utility:ensure-list
                          (cl-naive-dom::resolve-tag
                           'cl-naive-dom.tests::p nil
                           (list
                            "How is the testing going?")))))
            :actual (with-current-package
                      (parse
                       '(:div :class "section" :test "YES"
                         (:h1 "Testing Docs")
                         (:p "How is the testing going?"))))
            :info "Expanded results form parser.")

  (testcase :parse-with-variables-named-like-tags
            :expected '(let ((div section) (h1 "Testing Docs"))
                        (cl-naive-dom::resolve-tag 'div
                         (list :class div :test "YES")
                         (concatenate 'list
                          (uiop/utility:ensure-list (cl-naive-dom::resolve-tag 'h1
                                                     nil
                                                     (uiop/utility:ensure-list
                                                      h1)))
                          (uiop/utility:ensure-list (p "How is the testing going?")))))
            :actual (with-current-package
                      (parse
                       '(let ((div section)
                              (h1 "Testing Docs"))
                         (div :class div :test "YES"
                          (h1 h1)
                          (p "How is the testing going?")))))
            :info "Expanded results form parser.")

  (testcase :parse-with-shadowed-symbol-tag
            :expected '(flet ((h1 (title) (cl-naive-dom::resolve-tag 'h1 (list :class "BigTitle") (list title))))
                        (cl-naive-dom::resolve-tag 'div
                         (list :class "section" :test "YES")
                         (concatenate 'list
                          (uiop/utility:ensure-list (h1 "Testing Docs"))
                          (uiop/utility:ensure-list (cl-naive-dom::resolve-tag 'p
                                                     nil
                                                     (list
                                                      "How is the testing going?"))))))
            :actual (with-current-package
                      (parse
                       '(flet ((h1 (title)
                                (cl-naive-dom::resolve-tag
                                 'cl-naive-dom.tests::h1
                                 (list :class "BigTitle")
                                 (list title))))
                         (:div :class "section" :test "YES"
                          (h1 "Testing Docs")
                          (:p "How is the testing going?")))))

            :info "Expanded results form parser.")

  (testcase :parse-with-shadowed-keyword-tag
            :expected '(flet ((h1 (title) (cl-naive-dom::resolve-tag 'h1 (list :class "BigTitle") (list title))))
                        (cl-naive-dom::resolve-tag 'div
                         (list :class "section" :test "YES")
                         (concatenate 'list
                          (uiop/utility:ensure-list (cl-naive-dom::resolve-tag 'h1
                                                     nil
                                                     (list "Testing Docs")))
                          (uiop/utility:ensure-list (cl-naive-dom::resolve-tag 'p
                                                     nil
                                                     (list
                                                      "How is the testing going?"))))))
            :actual (with-current-package
                      (parse
                       '(flet ((h1 (title)
                                (cl-naive-dom::resolve-tag
                                 'cl-naive-dom.tests::h1
                                 (list :class "BigTitle")
                                 (list title))))
                         (:div :class "section" :test "YES"
                          (:h1 "Testing Docs")
                          (:p "How is the testing going?")))))

            :info "Expanded results form parser.")

  (testcase :with-sexp
            :expected '(:DIV (:H1 "Testing Docs") (:P "How is the testing going?"))
            :actual (with-current-package
                      (with-sexp
                        (:div
                         (:h1 "Testing Docs")
                         (:p "How is the testing going?"))))
            :info "Raw results from parser.")

  (testcase :dom
            :expected '(DIV 100 2 H1 ("Testing Docs") P ("How is the testing going?"))
            :actual (with-current-package
                      (let ((dom (with-dom
                                   (:div :width 100
                                         (:h1 "Testing Docs")
                                         (:p "How is the testing going?")))))
                        (list (tag dom)
                              (value (first (attributes dom)))
                              (length (children dom))
                              (tag (first (children dom)))
                              (children (first (children dom)))
                              (tag (second (children dom)))
                              (children (second (children dom))))))
            :info "Dom from DSL")

  (testcase :dom-dynamic-attribute
            :expected '(DIV "widt=ok" 2 H1 ("Testing Docs") P ("How is the testing going?"))
            :actual (with-current-package
                      (let ((dom (with-dom
                                   (:div :width (if (> (random 5) 2)
                                                    "long"
                                                    "short")
                                         (:h1 "Testing Docs")
                                         (:p "How is the testing going?")))))
                        (list (tag dom)
                              (and (member (value (first (attributes dom))) (list "long" "short")
                                           :test #'equal)
                                   "widt=ok")
                              (length (children dom))
                              (tag (first (children dom)))
                              (children (first (children dom)))
                              (tag (second (children dom)))
                              (children (second (children dom))))))
            :info "Dynamic attribute value.")

  (testcase :dom-lexical-binding-1
            :expected '(DIV 1 2 H1 ("Testing Docs") P ("How is the testing going?"))
            :actual (with-current-package
                      (let ((dom (with-dom
                                   (let ((x 1))
                                     (:div :width x
                                           (:h1 "Testing Docs")
                                           (:p "How is the testing going?"))))))
                        (list (tag dom)
                              (value (first (attributes dom)))
                              (length (children dom))
                              (tag (first (children dom)))
                              (children (first (children dom)))
                              (tag (second (children dom)))
                              (children (second (children dom))))))
            :info "Lexical binding.")

  (testcase :dom-lexical-binding-2
            :expected '(DIV 1 2 H1 ("Testing Docs") P ("How is the testing going?"))
            :actual (with-current-package
                      (let ((dom (let ((x 1))
                                   (with-dom
                                     (:div :width x
                                           (:h1 "Testing Docs")
                                           (:p "How is the testing going?"))))))
                        (list (tag dom)
                              (value (first (attributes dom)))
                              (length (children dom))
                              (tag (first (children dom)))
                              (children (first (children dom)))
                              (tag (second (children dom)))
                              (children (second (children dom))))))
            :info "Lexical binding.")

  ;;TODO: ???
  #-(and)
  (testcase :deftag
            :expected t
            :actual (equalp (format nil "~A" (type-of (cl-naive-dom:deftag (:custom ())
                                                        (:span "Custom Tag"))))
                            (format nil "~A" 'standard-method))
            :info "Define a tag.")

  (testcase :expand
            :expected '(DIV 1 SPAN ("Custom World Tag"))
            :actual (with-current-package
                      (let ((dom
                              (expand
                               (let ((x 1))
                                 (with-dom (:div
                                            (:custom :width x)))))))
                        (list (tag dom)
                              (length (children dom))
                              (tag (first (children dom)))
                              (children (first (children dom))))))
            :info "Expand custom tag.")

  ;;TODO: ???
  #-(and)
  (testcase :deftag-alt
            :expected t
            :actual (with-current-package
                      (equalp (type-of (deftag (:custom-alt ())
                                         (let ((body "Custom Tag - Alt"))
                                           (:div ; transforms must return an element.
                                            (:div "Multiple tag return.")
                                            (:span
                                             :atts% *attributes*
                                             body)))))
                              'standard-method))
            :info "Lexical binding.")

  (testcase :expand-alt
            :expected '(CL-NAIVE-DOM.TESTS::DIV 2 1
                        CL-NAIVE-DOM.TESTS::SPAN :WIDTH 1)
            :actual (with-current-package
                      (let ((dom (expand
                                  (with-dom
                                    (let ((x 1))
                                      (:custom-alt :width x))))))
                        (list (tag dom)
                              ;;custom-alt expand should return 2 children
                              (length (children dom))
                              (length (children (first (children dom))))
                              (tag (second (children dom)))
                              (key (first (attributes (second (children dom)))))
                              (value (first (attributes (second (children dom))))))))
            :info "Expand custom tag, test :atts%")

  (testcase :emit-html
            :expected "<div class=\"A\">
  <div>
    <div>
      Multiple tag return.
    </div>
    <span width=1>
      Custom Tag - Alt
    </span>
  </div>
</div>"
            :actual (with-current-package
                      (with-abt ()
                        (let ((x 1))
                          (:div :class "A"
                                (:custom-alt :width x)))))
            :info "Emit html, sink.")

  (testcase :dynamic-tag
            :expected "<h1>
  Heading
</h1>"
            :actual (with-current-package
                      (with-abt ()
                        (let ((x 1))
                          (:h :size x
                              "Heading"))))
            :info "Dynamic tag.")

  (testcase :dom-tag
            :expected "<div width=10>
  <span class=\"b\">
    Hello
    there!
  </span>
</div>"
            :actual (with-current-package
                      (let* ((text "Hello")
                             (width 10)
                             (class "b"))
                        (with-abt ()
                          (:custom-a :width width :class class text "there!"))))
            :info "Use dom to create tag expansion")

  (testcase :expand-lambda
            :expected "<div width=10>
  <span class=\"b\">
    Hello
    there!
  </span>
</div>"
            :actual (with-current-package
                      (let* ((text "Hello")
                             (width 10)
                             (class "b"))
                        (with-abt ()
                          (:custom-b :width width :class class text "there!"))))
            :info "Enviroment expand.")

  (testcase :expand-lambda-dom-tag
            :expected "<div width=10>
  <span class=\"b\">
    Hello
    there!
  </span>
</div>"
            :actual (with-current-package
                      (let* ((text "Hello")
                             (width 10)
                             (class "b"))
                        (with-abt ()
                          (:custom-c :width width :class class text "there!"))))
            :info "Enviroment expand widh dom buildt tag.")

  (testcase :test-big
            :expected 230999
            :actual (length
                     (with-current-package
                       (let ((*gensym-counter* 100))
                         (let ((text "Hello")
                               (width 10)
                               (class "b")
                               (elements))

                           (cl-naive-dom.abt:with-abt ()
                             (dotimes (i 1000)
                               (push
                                (:div
                                 (:span "a")
                                 (:span "b")
                                 (:custom-c :width width :class class text "there!")
                                 (:custom-c :width width :class class text "there!"))
                                elements))
                             elements)))))
            :info "Big html")

  (testcase :with-js
            :expected 268
            :actual (length (with-current-package
                              (let ((*gensym-counter* 100))
                                (let* ((text "Hello")
                                       (width 10)
                                       (class "b"))
                                  (with-js
                                    (:custom-b :width width :class class text "there!"))))))
            :info "Js to inject")

  (testcase :query-selector
            :expected 2
            :actual (length (with-current-package
                              (query-selector
                               (with-dom
                                 (:document
                                  :title "My CV"
                                  :last-update "2000"
                                  :name "Piet"
                                  :surname "Gieter"
                                  :jobs (list
                                         (:job
                                          :job-title "Bottle Washer"
                                          :when "1994"
                                          :duties (list "Washing bottles."
                                                        "Washing more bottles."))
                                         (:job
                                          :job-title "Chief Cook"
                                          :when "1994"
                                          :duties (list "Cook food."
                                                        "Cook more food.")))
                                  (:summary "Its been a colourful career of menial jobs")))
                               (lambda (element path)
                                 (declare (ignore path))
                                 (when (and (tag-equal-p element :job)
                                            (equal (attribute element :when) "1994"))
                                   element)))))
            :info "Query selector")

  (testcase :query-selector-1
            :expected 0
            :actual (length
                     (with-current-package
                       (let ((dom (with-dom
                                    (:resume :date "2022-08-26"
                                             (:name :date "1962-02-12" "James Tiberius Kirk")
                                             (:birthdate "2233/03/22")))))
                         (query-selector dom
                                         (lambda (element path &rest rest &key &allow-other-keys)
                                           (declare (ignore element path rest))
                                           nil)))))
            :info "Query selector 1")

  (testcase :query-selector-2
            :expected 3
            :actual (length
                     (with-current-package
                       (let ((dom (with-dom
                                    (:resume :date "2022-08-26"
                                             (:name :date "1962-02-12" "James Tiberius Kirk")
                                             (:birthdate "2233/03/22")))))
                         (query-selector dom
                                         (lambda (element path &key &allow-other-keys)
                                           (declare (ignore path))
                                           element)))))
            :info "Query selector 2")

  (testcase :query-selector-3
            :expected 2
            :actual (length
                     (let ((dom (with-dom
                                  (:resume :date "2022-08-26"
                                           (:name :date "1962-02-12" "James Tiberius Kirk")
                                           (:birthdate "2233/03/22")))))
                       (query-selector dom
                                       (lambda (element path &key &allow-other-keys)
                                         (declare (ignore path))
                                         (attribute element :date)))))
            :info "Query selector 3")

  (testcase :query-selector-4
            :expected t
            :actual (let* ((dom (with-dom
                                  (:resume :date "2022-08-26"
                                           (:name :date "1962-02-12" "James Tiberius Kirk")
                                           (:birthdate "2233/03/22"))))
                           (results (query-selector dom
                                                    (lambda (element path &key &allow-other-keys)
                                                      (declare (ignore path))

                                                      (if (search "date" (format nil "~A" (tag element)) :test #'equalp)
                                                          element
                                                          (list :date (attribute element :date)))))))
                      (and (listp (nth 0 results))
                           (listp (nth 1 results))
                           (typecase (nth 2 results)
                             (element
                              t)
                             (otherwise nil))))
            :info "Query selector 4")

  (testcase :persist-and-load
            :expected '(:DIV (:CUSTOM-ALT))
            :actual (with-current-package
                      (persist (with-dom
                                 (:div (:custom-alt)))
                               (temp-file "eish.dom"))

                      (with-sexp (load-dom (temp-file "eish.dom"))))
            :info "Query selector")
  (testcase :persist-and-load-expand
            :expected '(:DIV (:DIV (:DIV "Multiple tag return.") (:SPAN "Custom Tag - Alt")))
            :actual (with-current-package
                      (persist (with-dom
                                 (:div (:custom-alt)))
                               (temp-file "eish-expanded.dom"))
                      (with-sexp (expand (load-dom (temp-file "eish-expanded.dom")))))
            :info "Query selector"))

;;(report (run))

