(in-package :cl-naive-dom.tests)

(deftag (:cv ()))
(deftag (:name ()))
(deftag (:birthdate ()))
(deftag (:studies ()))
(deftag (:study ()))
(deftag (:school ()))
(deftag (:diploma ()))
(deftag (:achievement ()))
(deftag (:jobs ()))
(deftag (:job ()))
(deftag (:title ()))
(deftag (:grade ()))
(deftag (:description ()))
(deftag (:skills ()))
(deftag (:skill ()))
(deftag (:date ()))

(defparameter *kirk-resume*
  (with-dom
    (:cv
     (:name "James Tiberius Kirk")
     (:birthdate "2233/03/22")
     (:studies
      (:study
       (:school "Starfleet Academy")
       (:achievement "First and only student at Starfleet Academy to
defeat the Kobayashi Maru test, garnering a commendation for
original thinking after he reprogrammed the computer to make the
\"no-win scenario\" winnable.")))
     (:jobs
      (:job (:title "Security Officer")
            (:grade "Lieutenant")
            (:vessel "USS Farragut")
            (:description "Planetary surveys, survived deadly attack")
            (:skills (:skill "survival") (:skill "exploration")))
      (:job (:title "Ship Commandant")
            (:grade "Captain")
            (:vessel "USS Enterprise")
            (:description "Five Year Mission of Exploration.")
            (:skills (:skill "command") (:skill "survival") (:skill  "exploration")))
      (:job (:title "Chief of Starfleet Operation")
            (:grade "Admiral")
            (:description "Direction of all Starfleet Operations.")
            (:skills (:skill "command")))))))

(define-transform (gather-skills-1 (:cv) (element new-children))
  (make-instance 'element
                 :tag (tag element)
                 :attributes (attributes element)
                 :children
                 (cons (make-instance
                        'element
                        :tag :skills
                        :children (mapcar (function copy-element)
                                          (remove-duplicates (extract :skill element)
                                                             :test (function element-equal-p))))
                       new-children)))

(defun format-escape (string)
  (with-output-to-string (out)
    (loop :for ch :across string
          :if (char= ch #\~)
          :do (princ #\~ out)
              (princ #\~ out)
          :else
          :do (princ ch out))))

(defun join (strings &optional (separator "") (end ""))
  (format nil (format nil "~~{~~A~~^~A~~}~~A" (format-escape separator))
          strings end))

(defun format-skills (from-element)
  (join (mapcar (lambda (node) (join (children node) ", "))
                (remove-duplicates (extract :skill from-element)
                                   :test (function element-equal-p)))
        "; " "."))

(defun li-from-list (items)
  (mapcar (lambda (item) (with-dom (:li item))) items))

(define-transform (studies-to-html (:studies) (element new-children))
  (declare (ignore element))
  (:div
   (:h2 "Studies")
   (:ul
    (li-from-list new-children))))

(define-transform (study-to-html (:study) (element new-children))
  (declare (ignore new-children))
  (:div "At " (:b (join (children (first (extract :school element))) " "))
        ": " (join (children (first (extract :achievement element))) " ")))

(define-transform (jobs-to-html (:jobs) (element new-children))
  (declare (ignore element))
  (:div
   (:h2 "Jobs")
   (:ul
    (li-from-list new-children))))

(define-transform (job-to-html (:job) (job new-children))
  (declare (ignore new-children))
  (:div
   (:h3 (extract :title job))
   (:p (extract :description job))
   (:p "Grade: " (extract :grade job) ", Vessel: " (extract :vessel job))
   (:p "Skills: " (format-skills job))))

;; (pprint (parse '(:div
;;            (:h3 (extract :title job))
;;            (:p (extract :description job))
;;            (:p "Grade: " (extract :grade job) ", Vessel: " (extract :vessel job))
;;                  (:p "Skills: " (format-skills job)))))
;;
;; (pprint (parse '(:cv (:skills (mapcar (function copy-element)
;;                        (remove-duplicates (extract :skill element)
;;                                           :test (function element-equal-p))))
;;                  new-children)))

(define-transform (cv-to-html (:cv) (element new-children))
  (let ((name  (join (children (first (extract :name element))) " "))
        (skills (format-skills element)))

    (:html
     (:title (format nil "Resume of ~A" name))
     (:body
      (:h1 "Resume")
      (:h2 "Identity")
      (:dl (:dt "Name")     (:dd name)
           (:dt "Birthday") (:dd (extract :birthdate element)))
      (:h2 "Skills")
      (:p skills)
      (remove-if-not (lambda (child)
                       (tag-equal-p :div child))
                     new-children)))))

(defparameter *cv-to-html-chain* (list (transform study-to-html)
                                       (transform studies-to-html)
                                       (transform job-to-html)
                                       (transform jobs-to-html)
                                       (transform cv-to-html)))

(define-transform (gather-skills-2 (:cv) (element new-children))
  (:cv (:skills (mapcar (function copy-element)
                        (remove-duplicates (extract :skill element)
                                           :test (function element-equal-p))))
       new-children))

(defun skill-element-p (element &rest ignored)
  (declare (ignore ignored))
  (eql 'skill (tag element)))

(testsuite :transforms

  (testcase :extract/tag
            :expected '((:skill "survival")
                        (:skill "exploration")
                        (:skill "command")
                        (:skill "survival")
                        (:skill "exploration")
                        (:skill "command"))
            :actual (with-current-package
                      (mapcar (lambda (element)
                                (emit-element :sexp :skill element nil))
                              (extract :skill *kirk-resume*))))

  (testcase :extract/tag-selector
            :expected '((:skill "survival")
                        (:skill "exploration")
                        (:skill "command")
                        (:skill "survival")
                        (:skill "exploration")
                        (:skill "command"))
            :actual (with-current-package
                      (mapcar (lambda (element)
                                (emit-element :sexp :skill element nil))
                              (extract (selector :skill) *kirk-resume*))))

  (testcase :extract/tag-selector-multiple
            :expected '((:grade "Lieutenant")
                        (:skill "survival")
                        (:skill "exploration")
                        (:grade "Captain")
                        (:skill "command")
                        (:skill "survival")
                        (:skill "exploration")
                        (:grade "Admiral")
                        (:skill "command"))
            :actual (with-current-package
                      (mapcar (lambda (element)
                                (emit-element :sexp '(:grade :skill) element nil))
                              (extract (selector (:grade :skill)) *kirk-resume*))))

  (testcase :extract/function-selector
            :expected '((:skill "survival")
                        (:skill "exploration")
                        (:skill "command")
                        (:skill "survival")
                        (:skill "exploration")
                        (:skill "command"))
            :actual (with-current-package
                      (mapcar (lambda (element)
                                (emit-element :sexp :skill element nil))
                              (extract (selector skill-element-p) *kirk-resume*))))

  (testcase :extract/function-selector
            :expected '((:skill "survival")
                        (:skill "exploration")
                        (:skill "command")
                        (:skill "survival")
                        (:skill "exploration")
                        (:skill "command"))
            :actual (with-current-package
                      (mapcar (lambda (element)
                                (emit-element :sexp :cv element nil))
                              (extract :skill *kirk-resume*))))

  (testcase :perform-transform-1
            :expected '(:cv (:skills (:skill "survival") (:skill "exploration") (:skill "command")) (:name "James Tiberius Kirk")
                        (:birthdate "2233/03/22")
                        (:studies
                         (:study (:school "Starfleet Academy")
                          (:achievement "First and only student at Starfleet Academy to
defeat the Kobayashi Maru test, garnering a commendation for
original thinking after he reprogrammed the computer to make the
\"no-win scenario\" winnable.")))
                        (:jobs
                         (:job (:title "Security Officer") (:grade "Lieutenant") (:vessel "USS Farragut")
                          (:description "Planetary surveys, survived deadly attack") (:skills (:skill "survival") (:skill "exploration")))
                         (:job (:title "Ship Commandant") (:grade "Captain") (:vessel "USS Enterprise")
                          (:description "Five Year Mission of Exploration.")
                          (:skills (:skill "command") (:skill "survival") (:skill "exploration")))
                         (:job (:title "Chief of Starfleet Operation") (:grade "Admiral")
                          (:description "Direction of all Starfleet Operations.") (:skills (:skill "command")))))
            :actual (with-current-package
                      (emit-element :sexp :cv
                                    (perform-transform (transform gather-skills-1)
                                                       *kirk-resume*)
                                    nil)))

  (testcase :perform-transform-2
            :expected '(:cv (:skills (:skill "survival") (:skill "exploration") (:skill "command")) (:name "James Tiberius Kirk")
                        (:birthdate "2233/03/22")
                        (:studies
                         (:study (:school "Starfleet Academy")
                          (:achievement "First and only student at Starfleet Academy to
defeat the Kobayashi Maru test, garnering a commendation for
original thinking after he reprogrammed the computer to make the
\"no-win scenario\" winnable.")))
                        (:jobs
                         (:job (:title "Security Officer") (:grade "Lieutenant") (:vessel "USS Farragut")
                          (:description "Planetary surveys, survived deadly attack") (:skills (:skill "survival") (:skill "exploration")))
                         (:job (:title "Ship Commandant") (:grade "Captain") (:vessel "USS Enterprise")
                          (:description "Five Year Mission of Exploration.")
                          (:skills (:skill "command") (:skill "survival") (:skill "exploration")))
                         (:job (:title "Chief of Starfleet Operation") (:grade "Admiral")
                          (:description "Direction of all Starfleet Operations.") (:skills (:skill "command")))))
            :actual (with-current-package
                      (emit-element :sexp :cv
                                    (perform-transform (transform gather-skills-2)
                                                       *kirk-resume*)
                                    nil)))

  (testcase :perform-transform-3
            :expected '(:div (:h2 "CV") (:div (:div "NAME") (:div (:name "James Tiberius Kirk"))))
            :actual (with-current-package

                      (deftag :cv)

                      (define-transform (cv-to-html (:cv) (element new-children))
                        (declare (ignore new-children))
                        (:div
                         (:h2 "CV")
                         (:div
                          (:div "NAME")
                          (:div (extract :name element)))))
                      (emit-element :sexp :cv
                                    (let ((dom (with-dom
                                                 (:cv
                                                  (:name "James Tiberius Kirk")
                                                  (:birthdate "2233/03/22")))))
                                      (perform-transform 'cv-to-html dom))
                                    nil)))

  (testcase :perform-transform/no-such-transform
            :expected '(no-such-transform tintin-et-l-or-noir)
            :actual (with-current-package
                      (handler-case
                          (let ((dom (with-dom
                                       (:cv
                                        (:name "James Tiberius Kirk")
                                        (:birthdate "2233/03/22")))))
                            (perform-transform 'tintin-et-l-or-noir dom))
                        (:no-error (&rest results)
                          (list 'invalid-success results))
                        (no-such-transform (err)
                          (list 'no-such-transform (no-such-transform-name err)))
                        (error (err)
                          (list 'other-error err))))))

(testsuite :chains

  (testcase :basic-chain
            :expected '(:html (:title "Resume of James Tiberius Kirk")
                        (:body (:h1 "Resume") (:h2 "Identity")
                         (:dl (:dt "Name") (:dd "James Tiberius Kirk") (:dt "Birthday") (:dd (:birthdate "2233/03/22"))) (:h2 "Skills")
                         (:p ".")
                         (:div (:h2 "Studies")
                          (:ul
                           (:li
                            (:div "At " (:b "Starfleet Academy") ": " "First and only student at Starfleet Academy to
defeat the Kobayashi Maru test, garnering a commendation for
original thinking after he reprogrammed the computer to make the
\"no-win scenario\" winnable."))))
                         (:div (:h2 "Jobs")
                          (:ul
                           (:li
                            (:div (:h3 (:title "Security Officer")) (:p (:description "Planetary surveys, survived deadly attack"))
                             (:p "Grade: " (:grade "Lieutenant") ", Vessel: " (:vessel "USS Farragut"))
                             (:p "Skills: " "survival; exploration.")))
                           (:li
                            (:div (:h3 (:title "Ship Commandant")) (:p (:description "Five Year Mission of Exploration."))
                             (:p "Grade: " (:grade "Captain") ", Vessel: " (:vessel "USS Enterprise"))
                             (:p "Skills: " "command; survival; exploration.")))
                           (:li
                            (:div (:h3 (:title "Chief of Starfleet Operation")) (:p (:description "Direction of all Starfleet Operations."))
                             (:p "Grade: " (:grade "Admiral") ", Vessel: ") (:p "Skills: " "command.")))))))
            :actual (with-current-package
                      (emit-element :sexp :cv
                                    (apply-transform-chain *cv-to-html-chain* *kirk-resume*)
                                    nil)))

  (testcase :check-22-apply-transform-chain-second-in-chain-adding-extra-div
            :info "Validate the correction of the bug 22-apply-transform-chain-second-in-chain-adding-extra-div"
            :actual (with-current-package

                      (deftag :cv)

                      (define-transform (cv-to-html-step-1 (:cv) (element new-children))
                        (declare (ignore new-children))
                        (:html-with-just-name (extract :name element)))

                      (define-transform (cv-to-html-step-2 (:name) (element new-children))
                        (declare (ignore new-children))
                        (:div
                         (:h2 "CV")
                         element))

                      (define-transform (cv-to-html-step-3 (:html-with-just-name)
                                         (element new-children))
                        (declare (ignore element))
                        (first new-children))

                      (let ((dom (with-dom
                                   (:cv
                                    (:name "James Tiberius Kirk")
                                    (:birthdate "2233/03/22")))))

                        (list
                         (with-sexp
                           (perform-transform 'cv-to-html-step-1 dom))

                         (with-sexp
                           (apply-transform-chain '(cv-to-html-step-1
                                                    cv-to-html-step-2
                                                    cv-to-html-step-3)
                                                  dom)))))

            :expected '((:HTML-WITH-JUST-NAME (:NAME "James Tiberius Kirk"))
                        (:DIV (:H2 "CV") (:NAME "James Tiberius Kirk")))))

;;(report (run))
