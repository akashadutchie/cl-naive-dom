(defsystem "cl-naive-dom.js"
  :description "Simple tool to emit js that can be used to create browser dom elements."
  :version "2022.3.17"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:cl-naive-dom :parenscript :str)
  :components (
               (:file "src/js/package")
               (:file "src/js/js" :depends-on ("src/js/package"))))
