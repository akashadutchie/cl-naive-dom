(defsystem "cl-naive-dom.tests"
  :description "Tests for cl-naive-dom."
  :version "2022.9.5"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:cl-naive-dom
               :cl-naive-dom.abt :cl-naive-dom.js :cl-naive-tests
               :cl-fad)
  :components (
               (:file "tests/package")
               (:file "tests/tests"              :depends-on ("tests/package"))
               (:file "tests/test-transforms"    :depends-on ("tests/package" "tests/tests"))
               (:file "tests/test-double-expand" :depends-on ("tests/package" "tests/tests"))))

